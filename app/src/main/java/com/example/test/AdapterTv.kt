package com.example.test

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AdapterTv(val info:List<String>): RecyclerView.Adapter<AdapterTv.ViewHolder>() {
    class ViewHolder(v: View):RecyclerView.ViewHolder(v) {
        val hg = v.findViewById<TextView>(R.id.tv)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var v: View = LayoutInflater.from(parent.context).inflate(R.layout.ls,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int = info.size



    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.hg.text = info[position]
    }

}